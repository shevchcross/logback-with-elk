package com.conduent.databroker.lod_simple;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ErrorsAdvice {
    @ExceptionHandler(value = {PrintException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public Error handleLoginError(Exception e) {
        return new Error(e.getMessage());
    }
}
