package com.conduent.databroker.lod_simple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PrintException extends Exception {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrintException.class);

    private static final long serialVersionUID = 445670554417085824L;

    public PrintException(final String message, final int id) {
        super(message);

        LOGGER.error("Printing was failed. Message id : {}, Error message: {}", id, message);
    }
}