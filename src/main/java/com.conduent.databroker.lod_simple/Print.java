package com.conduent.databroker.lod_simple;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class Print {

    @Autowired
    RestTemplate restTemplete;

    @Bean
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    private static final Logger LOGGER	= LoggerFactory.getLogger( Print.class );


    @RequestMapping(value = "/simple")
    public String print( ) throws PrintException {
        String message ="message";
        for ( int i = 1; i <= 10; i++ ) {
        LOGGER.debug( "Message was received to print. Message : {}, Message id : {}", message, i );
        if ( i % 3 == 0 ) {
            throw new PrintException( "Message id can not be divided by three", i );
        }
        LOGGER.info( "Printing is success. Message id : {}", i);

    } return "success";
}}
